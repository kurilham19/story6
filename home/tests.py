from django.test import TestCase,Client
from django.urls import resolve
from .views import home

# Create your tests here.
class TestHome(TestCase):
	def test_home_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code,200)
	def test_home_using_to_do_list_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response,'home.html')
	def test_home_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, home)
